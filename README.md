# Koha plugin - Demo site

This plugin sets some _CSS_ to make it obvious it is a demo site.

## Install

Download the latest _.kpz_ file from the _Project / Releases_ page